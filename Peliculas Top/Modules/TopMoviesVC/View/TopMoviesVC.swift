//
//  TopMoviesVC.swift
//  Peliculas Top
//
//  Created by Neptali Duque on 10/23/18.
//  Copyright © 2018 Neptali Duque All rights reserved.
//

import UIKit

class TopMoviesVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let movieCellID = "MovieCell"
    var presenter: TopMoviesPresenter?
    var movieArray = [Movie]()

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setUpNavigationModule()
        presenter = TopMoviesPresenter(from: self)
        setupTable()
        
    }
    
    // Sets up the UITableView used
    func setupTable(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.registerCell(named: movieCellID)
    }
    
    // Sets up the navigation module
    func setUpNavigationModule(){
        
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem?.tintColor = UIColor.white
        
    }

}

extension TopMoviesVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let index = indexPath.row
        let newVC = ViewFactory.viewControllerForAppView(.movieDetail) as! MovieDetailVC
        newVC.movieToShow = movieArray[index]
        
        //Push to Switch to the new View Controller
        self.navigationController?.pushViewController(newVC, animated: true)
        self.navigationController?.navigationBar.isHidden = false
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: movieCellID) as! MovieCell
        let index = indexPath.row
        
        cell.setupCell(movie: movieArray[index])
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(Constants.cellHeight)
    }
    
}

// Protocol used to handle networking operations
extension TopMoviesVC: TopMoviesView {
    
    func presenterFailedFetchingData(_ message: String) {
        tableView.setEmptyMessage(message)
    }
    
    func presenterGetTopMovies(movies: [Movie]) {
        movieArray = movies
        tableView.reloadData()
    }   
        
}
