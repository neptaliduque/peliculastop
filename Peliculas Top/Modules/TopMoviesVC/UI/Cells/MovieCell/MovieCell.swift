//
//  MovieCell.swift
//  Peliculas Top
//
//  Created by Freddy Medina on 10/23/18.
//  Copyright © 2018 Freddy Medina. All rights reserved.
//

import UIKit
import SDWebImage

class MovieCell: UITableViewCell {
    
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var popularityLabel: UILabel!
    @IBOutlet weak var votesLabel: UILabel!
    
    func setupCell(movie: Movie){
        
        movieImage.sd_setImage(with: movie.posterURL)
        titleLabel.text = movie.title
        dateLabel.text = movie.formattedDate()
        contentLabel.text = movie.overview
        popularityLabel.text = movie.popularityToString()
        votesLabel.text = movie.votesToString()
        addShadowToView()
        
    }

        // Configure the view for the selected state
    
    
}
