//
//  TopMoviesPresenter.swift
//  Peliculas Top
//
//  Created by Freddy Medina on 10/24/18.
//  Copyright © 2018 Freddy Medina. All rights reserved.
//

import Foundation

protocol TopMoviesView : NSObjectProtocol {
    func presenterGetTopMovies(movies: [Movie])
    func presenterFailedFetchingData(_ message: String)
}

class TopMoviesPresenter {
    
    var view: TopMoviesView?
    
    init(from linkView: TopMoviesView){
        
        view = linkView
        getMovies()
        
    }
    
    func getMovies(){
        
        MovieServices.getMovieList(sucessBlock: { (movies) in
            print("Got Some Data")
            self.view?.presenterGetTopMovies(movies: movies)
        }) { (error) in
            print("Error: \(error!.description)")
            self.view?.presenterFailedFetchingData("🛠 Hey! We're Sorry...\nFailed to Fetch Data due to\nsome Internet Connection from your end! 🛠")
        }
        
    }
        
}
