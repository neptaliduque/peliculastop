//
//  MovieServices.swift
//  Peliculas Top
//
//  Created by Freddy Medina on 10/24/18.
//  Copyright © 2018 Freddy Medina. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class MovieServices {
    
    static func getMovieList(sucessBlock: @escaping(_ movies: [Movie]) -> Void, errorBlock: @escaping(_ error: String?) -> Void) -> Void {
        
        let path = "\(Constants.baseMovieURL)\(Constants.apiKey)"
        
        Alamofire.request(path, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .validate()
            .responseArray(keyPath: Constants.keyID)
        { (response: DataResponse<[Movie]>) in
            
            switch response.result {
                case .success(let movies):
                    sucessBlock(movies)
                    break
                case .failure(let error):
                    errorBlock(error.localizedDescription)
                    break
            }
            
        }
        
    }
       
}
