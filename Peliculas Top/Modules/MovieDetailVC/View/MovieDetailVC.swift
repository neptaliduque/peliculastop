//
//  MovieDetailVC.swift
//  Peliculas Top
//
//  Created by Freddy Medina on 10/24/18.
//  Copyright © 2018 Freddy Medina. All rights reserved.
//

import UIKit
import SDWebImage

class MovieDetailVC: UIViewController {
    
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var popularityLabel: UILabel!
    @IBOutlet weak var votesLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var contentImage: UIView!
    
    var movieToShow: Movie = Movie()
    var presenter: MovieDetailPresenter?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        presenter = MovieDetailPresenter(from: self)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        if isMovingFromParent {
            self.navigationController?.navigationBar.isHidden = true
        }
        
    }

}

extension MovieDetailVC: MovieDetailsView{
    
    func loadMovieDetails() {
        
        updateNavBar()

        bannerImage.sd_setImage(with: movieToShow.posterDetailURL)
        contentImage.addShadowToView()
        
        popularityLabel.text = movieToShow.popularityToString()
        votesLabel.text = movieToShow.votesToString()
        contentLabel.text = movieToShow.overview
        dateLabel.text = movieToShow.formattedDate()
        
    }
    
    func updateNavBar(){
        
        self.navigationItem.title = movieToShow.title
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.white,
             NSAttributedString.Key.font: UIFont(name: "raleway-black", size: 19)!]
        
    }
    
}
