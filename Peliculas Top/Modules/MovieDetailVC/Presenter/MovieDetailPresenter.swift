//
//  MovieDetailPresenter.swift
//  Peliculas Top
//
//  Created by Freddy Medina on 10/24/18.
//  Copyright © 2018 Freddy Medina. All rights reserved.
//

import Foundation

protocol MovieDetailsView {
    func loadMovieDetails()
}

class MovieDetailPresenter {
    
    var view: MovieDetailsView?
    
    init(from view: MovieDetailsView) {
        self.view = view
        startLoad()
    }
    
    func startLoad(){
        self.view?.loadMovieDetails()
    }
    
}
