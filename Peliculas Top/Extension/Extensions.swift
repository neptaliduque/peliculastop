//
//  Extensions.swift
//  Shophisticated
//
//  Created by Gabriel Colmenares on 8/9/18.
//  Copyright © 2018 ShokWorks. All rights reserved.
//

import Foundation
import UIKit

import Foundation

// MARK: - UIView
extension UIView {
    
    // MARK: - Inspectables
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor {
        get {
            return self.borderColor
        }
        set {
            layer.borderColor = newValue.cgColor
        }
    }
    
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var circular: Bool {
        get {
            return self.circular
        }
        set {
            if newValue {
                self.layer.cornerRadius = self.frame.size.width/2
                self.clipsToBounds = true
            }
        }
    }
    
    func addBottomShadow() {
        let shadowPath = UIBezierPath()
        // Creating bottom shadow
        shadowPath.move(to: CGPoint(x: self.bounds.origin.x, y: self.bounds.size.height))
        shadowPath.addLine(to: CGPoint(x: self.bounds.width / 2, y: self.bounds.height + 4.0))
        shadowPath.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height))
        shadowPath.close()
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOpacity = 1
        self.layer.masksToBounds = false
        self.layer.shadowPath = shadowPath.cgPath
        self.layer.shadowRadius = 5
    }
    
    func addShadowToView(){
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 2
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 6
        self.clipsToBounds = false
        
    }
    
}

// MARK: - UIColor
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0) {
        self.init(
            red: CGFloat(red) / 255.0,
            green: CGFloat(green) / 255.0,
            blue: CGFloat(blue) / 255.0,
            alpha: a
        )
    }
    
    convenience init(rgb: Int, a: CGFloat = 1.0) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF,
            a: a
        )
    }
}

// MARK: - String
extension String {
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func trim() -> String {
        return self.replacingOccurrences(of: " ", with: "")
    }
    
    enum RegularExpressions: String {
        case phone = "^\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$"
    }
    
    func isValid(regex: RegularExpressions) -> Bool {
        return isValid(regex: regex.rawValue)
    }
    
    func isValid(regex: String) -> Bool {
        let matches = range(of: regex, options: .regularExpression)
        return matches != nil
    }
    
    func onlyDigits() -> String {
        let filtredUnicodeScalars = unicodeScalars.filter{CharacterSet.decimalDigits.contains($0)}
        return String(String.UnicodeScalarView(filtredUnicodeScalars))
    }
    
    func makeACall() {
        if isValid(regex: .phone) {
            if let url = URL(string: "tel://\(self.onlyDigits())"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
}


// MARK: - UITableView
extension UITableView {
    
    //This method allow to register cells ONLY of the
    //.xib and the cell class has the same name
    
    func registerCell(named: String) {
        let identifier = named
        let nib = UINib(nibName: identifier, bundle: nil)
        self.register(nib, forCellReuseIdentifier: identifier)
    }
    
    func setEmptyMessage(_ message: String){
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .white
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "Raleway-Black", size: 20)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel
        self.separatorStyle = .none
        
    }
}


// MARK: - UICollectionView
extension UICollectionView {
    
    //This method allow to register cells ONLY of the
    //.xib and the cell class has the same name
    
    func registerCell(named: String) {
        let identifier = named
        let nib = UINib(nibName: identifier, bundle: nil)
        self.register(nib, forCellWithReuseIdentifier: identifier)
    }
}


// MARK: - UIViewController
extension UIViewController {
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
//    var hideKeyboardOnTap: Bool {
//        set {
//            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
//            view.addGestureRecognizer(tap)
//            
//            if !newValue {
//                view.removeGestureRecognizer(tap)
//            }
//        }
//        get {
//            return self.hideKeyboardOnTap
//        }
//    }
    
//    func showAlertSingleOption (_ title: String?, message: String, handler: (()-> Void)?) {
//        let alert = UIAlertController (title: title, message: message, preferredStyle: UIAlertController.Style.alert)
//        let action = UIAlertAction (title: Localizables.Alerts.accept, style: .default, handler: { (action) in
//            if let handler = handler {
//                handler ()
//            }
//        })
//        alert.addAction(action)
//        present(alert, animated: true, completion: nil)
//    }
    
//    func shareActionWasSuccessfull() {
//        let localizables = Localizables.SharePrompt.self
//        showAlertSingleOption("", message: localizables.message, handler: nil)
//    }
    
}

// MARK: - UIImage
extension UIImage {
    
    func resize(newWidth: CGFloat) -> UIImage {
        let scale = newWidth / self.size.width
        let newHeight = self.size.height * scale
        let rect = CGRect(x: 0, y: 0, width: newWidth, height: newHeight)
        if #available(iOS 10.0, *) {
            return UIGraphicsImageRenderer(size: rect.size).image { _ in
                self.draw(in: rect)
            }
        } else {
            return resizeImage(image: self, newWidth: 500)
        }
    }
    
    private func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}


// MARK: - UITextField
extension UITextField {
    
    func setBottomBorder(withColor color: UIColor, width: CGFloat) {
        self.borderStyle = UITextField.BorderStyle.none
        self.backgroundColor = UIColor.clear
        
        let borderLine = UIView(frame: CGRect(x: 0, y: self.frame.height - width, width: self.frame.width, height: width))
        borderLine.backgroundColor = color
        self.addSubview(borderLine)
    }

}


// MARK: - Array
extension Array {
    func randomItem() -> Element? {
        if isEmpty { return nil }
        let index = Int(arc4random_uniform(UInt32(self.count)))
        return self[index]
    }
}
