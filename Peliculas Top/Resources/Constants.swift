//
//  Constants.swift
//  Peliculas Top
//
//  Created by Freddy Medina on 10/24/18.
//  Copyright © 2018 Freddy Medina. All rights reserved.
//

import Foundation

struct Constants {
    
    // --- Networking ---
    static let baseMovieURL = "http://api.themoviedb.org/3/movie/top_rated?api_key="
    static let baseImageMovieURL = "http://image.tmdb.org/t/p/"
    static let apiKey = "a1ae624ccb0a7b33f45b521d1e2681b4"
    static let keyID = "results"
    
    // TableViewCell
    static let cellHeight = 188.0
    
}
