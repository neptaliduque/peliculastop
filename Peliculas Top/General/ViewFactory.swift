//
//  ViewFactory.swift
//  Peliculas Top
//
//  Created by Freddy Medina on 10/24/18.
//  Copyright © 2018 Freddy Medina. All rights reserved.
//

import UIKit

class ViewFactory: NSObject {
    
    enum AppView : Int {
        case topMovies
        case movieDetail
    }
    
    class func viewControllerForAppView(_ view: AppView) -> UIViewController{
        
        switch view {
            
        case .topMovies:
            let topMoviesVC = TopMoviesVC(nibName: "TopMoviesVC", bundle: nil)
            let currentVC = UINavigationController(rootViewController: topMoviesVC)
            currentVC.navigationBar.isHidden = true
            return currentVC
            
        case .movieDetail:
            let movieDetailVC = MovieDetailVC(nibName: "MovieDetailVC", bundle: nil)
            return movieDetailVC
            
        default:
            return UIViewController()
            
        }
        
        
    }
}
