//
//  Movie.swift
//  Peliculas Top
//
//  Created by Freddy Medina on 10/24/18.
//  Copyright © 2018 Freddy Medina. All rights reserved.
//

import Foundation
import ObjectMapper

struct MovieJSON {
    
    static let title = "title"
    static let releaseDate = "release_date"
    static let overview = "overview"
    static let popularity = "popularity"
    static let voteAverage = "vote_average"
    static let posterPath = "poster_path"
    
}

struct ImageSizes {
    
    static let w185 = "w185///"
    static let w342 = "w342///"
    static let w500 = "w500///"
    
}

struct MonthHolder {
    
    static let Months : [String] = [
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre"
    ]
    
}

class Movie: NSObject, Mappable{
    
    var title: String = ""
    var releaseDate: String = ""
    var overview: String = ""
    var popularity: Float = 0.0
    var voteAverage: Float = 0.0
    var posterPath: String = ""
    
    var posterURL: URL? {
        let newUrl = "\(Constants.baseImageMovieURL)\(ImageSizes.w185)\(posterPath)"
        return URL(string: newUrl)
    }
    
    var posterDetailURL: URL? {
        let newUrl = "\(Constants.baseImageMovieURL)\(ImageSizes.w342)\(posterPath)"
        return URL(string: newUrl)
    }
    
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func popularityToString() -> String {
        return "Popularidad: \(String.init(format: "%.2f", popularity))"
    }
    
    func votesToString() -> String {
        return "Votos: \(voteAverage)"
    }
    
    func formattedDate() -> String {
        let date = releaseDate.components(separatedBy: "-")
        let month = MonthHolder.Months[Int(date[1])! - 1]
        
        return "\(date[2]) de \(month) de \(date[0])"
    }
    
    
    func mapping(map: Map) {
        title <- map[MovieJSON.title]
        releaseDate <- map[MovieJSON.releaseDate]
        overview <- map[MovieJSON.overview]
        popularity <- map[MovieJSON.popularity]
        voteAverage <- map[MovieJSON.voteAverage]
        posterPath <- map[MovieJSON.posterPath]
    }
    
    
}
